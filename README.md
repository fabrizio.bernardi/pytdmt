# Python  Time Domain Moment Tensor Inversion

    Type: ./pytdmt.py -h or --help for guide and examples

##### Current Version
V2.6.4 Roma 10.05.2016

tested: OSX10.11.5

require python 2.7

##### Old Version
V2.6.3 Roma 12.11.2015

tested: OSX10.9 OSX10.10, OSX10.11

require python 2.7

## A. Install

Requirements:

* Obspy        see http://obspy.org  
* rdseed-5.0   see http://www.iris.edu/forms/rdseed_request.htm
* FKRPROG      into src/fkrprog of this package Generate Frequency waves number for greens
* earth-models see earth-models/ of this package of this package
 for PREM,AK135 and Central Italy models
                         

* FKRPROG:      
    * cd src/fkrpog
    * gfortran -O3 FKRPROG.f -o FKRPROG or g77 -ff90 -O3 FKRPROG.f -o FKRPROG
    * mv FKRPROG ../../bin

Add Into your .profile or .bashrc or bash_profile depending on your OS:    

*  PATH=$PATH:/pathTo/tdmt/bin
*  PATH=$PATH:/pathTo/tdmt/py

add following lines to your profile

    PYTDMT_LIB=/data/fabrizio/lib
    PYTDMT_BIN=/home/fabrizio/gitlab/pytdmt/bin
    PYTDMT_PY=/home/fabrizio/gitlab/pytdmt/py
    PYTDMT_EMODELS=/home/fabrizio/gitlab/pytdmt/earth-models
    export PYTDMT_LIB
    export PYTDMT_BIN
    export PYTDMT_PY
    export PYTDMT_EMODELS


## B. Main executables

#### pytdmt.py
Perform moment tensor inversion

#### run\_pytdmt\_procedure.py
Perform automatic moment tensor inversion over a set of depths

Update pytdmt executable path at line 604

To plot reference gcmt solution, use single event in ndk file format.


## C. Syntetics
Pre-computed green's libraryes can be produced using the python script:

	synt/do_make_lib_synt.pl
	
which calls for different depths and distances the script 

	synt/do_synt_lib.py

Pre-computed libraries are available for PREM, AK135 and IASP91 Earth's models.
All greens are computed using FKRPROG.
Scripts to compute greens using the normal mode summation approach are already but with bugs. Such approach will be reliable in a next release of the software. 
Generally FKRPROG computed green's are reliable for local to regional epicentral distance (closer than 10 degree); normal mode summation computed green's are reliable for regional to teleseismic distances.

## D. Examples
Example for the Northern Italy 29.05.2012 Mw=5.8 earthquake.

	cd examples
	tar -xvzf 20120529070003.tgz
	cd 20120529070003

Single mt inversion with 2 iteration.
	
	./do_single_run.bash
	
Automatic mt inversion, best fit solution using fixed depth

	./do_automatic_procedure_single_depth.bash
	
Automatic mt inversion, best fit solution sampling different depths

	./do_automatic_procedure_sampling_depth.bash
	


## E. references
1. DUNKIN J. W. (1965),BSSA,335-358   
2. The Use of Preliminary First-Motion Mechanisms and Later Moment Tensor Solutions for Rapid Tsunami Early-Warning Scenario Forecasting, Geophysical Research Abstracts Vol. 18, EGU2016-15734, 2016 EGU General Assembly 2016
3. HASKELL N. A. (1964),BSSA,337-393   
4. WANG AND HERRMANN (1980),BSSA,1015-1036   
5. WATSON T. H. (1970),BSSA,161-166.
6. http://seismo.berkeley.edu/~dreger/mtindex.html
   (Dreger and Helmberger, 1993; Dreger, 2003)

