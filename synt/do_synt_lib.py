#!/opt/local/bin/python2.7


import os
import sys
import stat
import os.path
import argparse
from makeLibGreen import generateGreens
from readGreens import aquireGreens
from filterSeismograms import filtering

def parseMyLine():
    
    parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter,description='Synthetics generator\n------------\n')
    parser.add_argument('--model',default='None',help='Earth model for greenfunctions. Default=None. See README_MODEL.txt for details')
    parser.add_argument('--depth',default='10',help='Source depth. Default=10')

    parser.add_argument('--npts',default='512',help='Number of points for greens. Power of 2. Default=1024')
    parser.add_argument('--delta',default='0.5',help='Sampling interval in seconds for greens. Default=0.5')
    parser.add_argument('--cpus',default='1',help='Number of CPU available for greens computation. Min=1, Max=4. Default=1')
    parser.add_argument('--rvel',default='8',help='Reduction velocity. Recommendet value = 8km/s. Default=8')

    parser.add_argument('--libpath', default='pytdmt', help='Main lib directory. Default: $PATH_TO_PYTDMT/lib')
    parser.add_argument('--range', default='100 200', help='Distance range for syntetics in km. Default=100 200')
    parser.add_argument('--dkm', default='10', help='Distance intervall for syntetics in km. Default=10')

    parser.add_argument('--filtered',default='N', help='Apply taper and filter to the syntetics. \
                           !!! When Y these parameters must be used in pytdmt analysis. Default=Y')
    parser.add_argument('--bandpass',default='0', help='Bandpass filter "corners fimn fmax". No Defaults. E.g.: "2 0.01 0.1"')
    parser.add_argument('--highpass',default='0', help='Highpass filter "corners freq". No Defaults. E.g.: "2 0.01"')
    parser.add_argument('--lowpass',default='0', help='Lowpass filter "corners freq". No Defaults. E.g.: "2 0.1"')
    parser.add_argument('--zeroph',default='False', help='Zerophase for high, low and bandpass. True/False. Defaul:False')
    parser.add_argument('--taper',default='0.1', help='cos Taper. If taper=-1 no taper is applied. Defaults=0.1')
    parser.add_argument('--sim',default='PZs', help='Remove instrument method: PZs for poles and zeros, RESP, for RESP_ files. Default=PZs')
    parser.add_argument('--flim',default='0.002 0.005 0.5 1', help='Corner frequency for deconvolution filtering. Defaults 0.002 0.005 0.5 1')
    parser.add_argument('--dva',default='1', help='Data type: 1(displacement); 2(velocity); Default = 1')
    parser.add_argument('--pre', default='0', help='Length of signal in seconds Before event origin Time.')

    args=parser.parse_args()

    return args

def checkConsistency(self):

    if(self.bandpass != '0'):
       li = self.bandpass.split()
       if(li[2] <= li[1]):
          print "bandpass filter: Use nrCorners min_freq max_freq"
          sys.exit()

    if self.model != "None":
       aa=os.path.exists(self.model)
       if aa == False:
          print "Model --model", self.model, "does not exists"
          sys.exit()
    else:
       print "Model file unknown. Use --model option"
       sys.exit()

def rangeToList(args):

    D    = 0
    kms  = []
    dkm  = int(float(args.dkm)) * 1
    dist = args.range.split()
    dmin = int(float(dist[0])) * 1
    dmax = int(float(dist[1])) * 1 
    D    = int(float(dist[0])) * 1
    while D <= dmax:
          kms.append(D)
          D = D + dkm

    return kms

def which(name):
    found = 0 
    out_path = 'none'
    for path in os.getenv("PATH").split(os.path.pathsep):
        full_path = path + os.sep + name
        if os.path.exists(full_path):
            """
            if os.stat(full_path).st_mode & stat.S_IXUSR:
                found = 1
                print(full_path)
            """
            found = 1
            out_path = full_path
            break

    return full_path

def doFinalLibPart(args):

    # If args.libpath == pytdmt libraries are installed into
    # the actual runin pytdmt libarry
    # 
    mainPath = which('pytdmt.py')
#   print "----",mainPath
    mainPath = mainPath.replace(os.sep + 'py' + os.sep + 'pytdmt.py','')
    if (args.libpath == 'pytdmt'):
       mainPath = mainPath + os.sep + 'lib'
    else:
       mainPath = mainPath + os.sep + args.libpath
        
    # take model earth
    foe = args.model.split('/')
    EModel = foe[-1]

    # create main lib if not exists
    if (os.path.exists(mainPath) == False):
       try:
         os.makedirs(mainPath)
       except:
         print "Unable to create directory " + mainPath

    # generate work temp-subpath
#   print mainPath
    if (os.path.exists(mainPath + os.sep + 'WORK') == False):
       try:
         os.makedirs(mainPath + os.sep + 'WORK')
       except:
         print "Unable to create directory " + mainPath + os.sep + 'WORK'

#   print mainPath + os.sep + 'WORK'
     
    # based on the greens parameters (delta, npts, dkm, depth .. etc,
    # create subdirectories
    # structure
    # Earth model (PREM)
    # filter(unfiltered/001-002)
    # depth (010)
    # npts  (02048)
    # delta 0050
    if (args.filtered != 'Y'):
       f_mod = 'unfiltered'
    else:
       fee = args.bandpass.split()
       f_mod = "%1d-%.3f-%.3f" % (int(fee[0]),float(fee[1]), float(fee[2]))
    d_mod = "%03d" % (depth)
    n_mod = "%05d" % (npts)
    D_mod = "%04d" % (delta * 100)
    p_mod = "%03d" % (int(args.pre))

    subPath = EModel + os.sep + f_mod + os.sep + d_mod + os.sep + n_mod + os.sep + p_mod + os.sep + D_mod

    print mainPath + os.sep + subPath

    return mainPath, subPath
    

#####################################################################
#####################################################################
args=parseMyLine()
ll = sys.argv[1:]
if not ll:
       print "Use -h or --help option for Help"
       sys.exit(0)

checkConsistency(args)

# define main parameters:
delta      = float(args.delta)
nrcpu      = float(args.cpus)
npts       = float(args.npts)
rvel       = float(args.rvel)
depth      = float(args.depth)
earthModel = args.model

# Find FKPROG
fkprog_exec = which('FKRPROG')

# take model earth
foe = args.model.split('/')
EModel = foe[-1]

dist_list = rangeToList(args)

#dist_list = [100, 200, 300]
print dist_list

LibPath, finalLibPath = doFinalLibPart(args)

green = generateGreens(dist_list, depth, args.model, npts, nrcpu, delta, rvel, LibPath, fkprog_exec)

green = aquireGreens(green, args)

green = filtering(green,'g',args)

# Generate path
if (os.path.exists(LibPath + os.sep + finalLibPath) == False):
   try:
      os.makedirs(LibPath + os.sep + finalLibPath)
   except:
      print "Unable to create directory " + LibPath + os.sep + finalLibPath

for i in range(len(green)):
    d_name = "%03d" % (depth)
    n_name = "%05d" % (int(green[i].stats.npts))
    D_name = "%06.2f" % (float(green[i].stats.delta))
    p_name = "%03d" % (int(args.pre))
    dkm    = "%08.2f" % float(green[i].stats.dist)
    Name   = 'green' + '.' + EModel + '.Depth-' + d_name + '-.Npts-' + n_name + '-.Delta-' + D_name + '-.Dist-' + dkm + '-.' + green[i].stats.channel + '.sac'
    green[i].write(LibPath + os.sep + finalLibPath + os.sep + Name, format='SAC') 
#   print green[i].stats.npts, green[i].stats.delta, green[i].stats.channel, green[i].stats.dist, finalLibPath + os.sep + Name
