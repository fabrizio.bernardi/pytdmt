#!/bin/bash

EPI=`cat event.info | awk '{print $3, $4, $5}'`
ORI=`cat event.info | awk '{print $2}'`
IDM=`pwd | awk -F\/ '{print $NF}'`
TIT=`cat event.info |  cut -c166-190`
DEP=`echo $EPI | awk '{print $3}'` 

if [ -d "inv" ]; then
    rm -r "inv"
fi

time $PYTDMT_PY/run_pytdmt_procedure.py --epi "$EPI" --len 600 --pre 120 --npts 4096 --ori "$ORI" --title "$TIT" --bandpass "2 0.007 0.02" --model /usr/local/share/earth-models/PREM --pltname run1 --mseed wave/waveform.mseed --Xml xml --sim XML --stsC $IDM\_stations.txt --range "200 1000" --sort "A D C" --clean 1 --nrIter -1 --plt N --flim "0.002 0.005 0.1 0.5" --vr 30 --lib Y --minZcor -300 --maxZcor 300 --fix_depth $DEP --gcmt cmt_selected.ndk --noise 0.05 --sig2noise 1.1 --sig2noiselevel2 2 --purgeFileList $IDM\_exclude_stations.txt --purge "ELL RMP MDUB BLY" --subwid 4 --wid 0
