import numpy as np
from processData import getSTationslist
from obspy.core import Stream, read,UTCDateTime
from scipy.optimize import curve_fit
from pylab import plot,show
from obspy.signal.cross_correlation import xcorr
import sys

def getVarianceVar(st):

    vec = np.zeros(len(st)/3)

    for i in range(len(st)/3):
        vec[i] = float(st[i*3+0].stats.VR)

    return vec

def FindSpike(vec):

    # initialize empty vector, same dimension as vec
    d = np.zeros(len(vec))
    o = np.array(vec)

    # mean vector 
    mean=np.mean(vec)
    std=np.std(vec)

    pos = mean+std

    d = vec / mean

    m=min(d)
    ll=[i for i, j in enumerate(d) if j == m]

    mi = 0
    pi = 0
    mi = (vec < -10000).sum()
    pi = (vec >= -10).sum()

    if(abs(mean) >= 10000 and mi > pi):
      purge = True
    else:
      purge = False

    return ll, d[ll], o[ll], purge

def purgeStream(st,l):

    # Parameters: st: Stream, l: linst with station number [0 to nr stations]

    new=Stream()

    ls_Sta = getSTationslist(st)
    for i in range(len(l)):
        a = l[i]-i
        ls_Sta.pop(a)

    for i in range(len(st)):
        sta = st[i].stats.station
        rem = [y for y in ls_Sta if sta == y]
        if (len(rem)==1):
           new.append(st[i])
    
    return new

def getStat1(tr, a, b, c, args):

    status = False

    maxS = float(args.sig2noise) 

    #allocate first segment into numpy
    seg1 = tr.data[a:b]
    seg2 = tr.data[b:c]
#   print " ----1", tr.stats.station, tr.stats.channel, a, b, c, seg1[0:10]
#   print " ----2", tr.stats.station, tr.stats.channel, a, b, c, seg2[0:10]

    if(len(seg2) >= 60):
       max_seg1  = max(abs(seg1))
       max_seg2  = max(abs(seg2))
       mean_seg1 = abs(seg1.mean())
       mean_seg2 = abs(seg2.mean())
       std_seg1  = seg1.std()
       std_seg2  = seg2.std()

       aa = "%.1f %.1f %.1f" % ((max_seg2/max_seg1), (mean_seg2/mean_seg1), (std_seg2/std_seg1))
       M1 = "%.2f" % (max_seg2/max_seg1)
       M2 = "%.2f" % (mean_seg2/mean_seg1)
       M3 = "%.2f" % (std_seg2/std_seg1)

       if((max_seg2 / max_seg1) <= maxS):
#      if((max_seg2 / max_seg1) <= maxS or \
#      (std_seg2 / std_seg1) <= maxS):
#      (mean_seg2 / mean_seg1) <= maxS or \

          status = True
    else:
       M1 = 0
       M2 = 0
       M3 = 0
#   print (tr.stats.station, tr.stats.channel, aa, max_seg2, max_seg1, status)

    return status, float(M1), float(M2), float(M3) 


def cleanNoise(ob,args):

#   for i in range(len(ob)):
#       print "---", ob[i].stats.station, ob[i].stats.channel
#   sys.exit()

    ListN = []
    List  = []
    SubList  = []
    Max_noise = float(args.noise)
    sig2noise = float(args.sig2noise)
    if (Max_noise > 0):

      for i in range(len(ob)/3 + 0):

        # set temporary data vectors
        r = np.zeros(len(ob[i*3+0])/3)
        t = np.zeros(len(ob[i*3+1])/3)
        z = np.zeros(len(ob[i*3+2])/3)

        # set noise (random uniform) for reference
        n = np.random.uniform(-1,1,len(ob[i*3+0]))
        w = len(n)/4

        # Allocate data values
        r = ob[i*3+0].data/max(ob[i*3+0].data)
        t = ob[i*3+1].data/max(ob[i*3+1].data)
        z = ob[i*3+2].data/max(ob[i*3+2].data)

        # 3 comp reference values
        N_r = xcorr(n,r,w) 
        N_t = xcorr(n,t,w) 
        N_z = xcorr(n,z,w) 
        # find for
        R_t = xcorr(r,t,w)
        R_z = xcorr(r,z,w)
        T_z = xcorr(t,z,w)

        M_Rp = (R_t[0] + R_z[0] + T_z[0])/3.
        M_Np = (N_t[0] + N_z[0] + N_z[0])/3.
        M_Rv = (R_t[1] + R_z[1] + T_z[1])/3.
        M_Nv = (N_t[1] + N_z[1] + N_z[1])/3.

        
        if (abs(M_Rv) < Max_noise):
           ListN.append(i)
   
    if (len(ListN) > 0):
         ListN = list(set(ListN))
     

         listToRemove = ' '.join(['%d:%s' % (ListN[n],ob[ListN[n]*3].stats.station) for n in xrange(len(ListN))])
         print "\n\nStation to clean because of noise: ",listToRemove,"\n\n"

         # Purge station from observed
         ob  = purgeStream(ob,ListN)
         if(len(ob)==0):
            print "No data left. No solution found. Exit"
            sys.exit()

    List = []
    STAZ = []
    COMP = []
    MAXR = []
    MAXT = []
    MAXZ = []
    STDR = []
    STDT = []
    STDZ = []
    MEAR = []
    MEAT = []
    MEAZ = []

    if (sig2noise > 0 and len(ob) >0):

      dt = float(ob[0].stats.delta)
      pp = float(ob[0].stats.npts)
      t0 = ob[0].stats.starttime
      t1 = UTCDateTime(args.ori)
      t2 = ob[0].stats.endtime
      T0 = UTCDateTime(t0)
      T1 = UTCDateTime(t1)
      T2 = UTCDateTime(t2)

      if(float(ob[0].stats.dist) <= 500):
          vreduction = 5.0
      else:
          vreduction = 6.0

      vreduction = float(args.vred) 

      for i in range(len(ob)/3):
 
#       if(float(ob[i*3+0].stats.dist) <= 500):
#          vreduction = 5.0
#       else:
#          vreduction = 6.0

        T_dist = float(ob[i*3+0].stats.dist) /vreduction

        n0 = int(0)
        n1 = int(((t1+T_dist)-t0) / dt)
        n2 = int((t2-t0) / dt)

        stat_r, max_r, mean_r, std_r = getStat1(ob[i*3+0], n0, n1, n2, args)
        stat_t, max_t, mean_t, std_t = getStat1(ob[i*3+1], n0, n1, n2, args)
        stat_z, max_z, mean_z, std_z = getStat1(ob[i*3+2], n0, n1, n2, args)

        if(stat_r == True or stat_t == True or stat_z == True):
           List.append(i)
        else:
           STAZ.append(ob[i*3+0].stats.station)
           COMP.append(ob[i*3+0].stats.channel)
           MAXR.append(max_r)
           MAXT.append(max_t)
           MAXZ.append(max_z)
           STDR.append(std_r)
           STDT.append(std_t)
           STDZ.append(std_z)
           MEAR.append(mean_r)
           MEAT.append(mean_t)
           MEAZ.append(mean_z)


    if(float(args.sig2noiselevel2) > 0 and len(ob) >0):

       SubStaz = []
       level = float(args.sig2noiselevel2)
       newM = []
       newM.extend(MAXR)
       newM.extend(MAXT)
       newM.extend(MAXZ)
       newM = np.array(newM)
       if(len(newM) >= 5):
         M    = newM.mean()
         S    = newM.std()

         for i in range(len(MAXR)):
           dada = M + level*S
           if(MAXR[i] >= M + level*S or MAXT[i] >= M + level*S or MAXZ[i] >= M + level*S):
              SubList.append(i)
              SubStaz.append(STAZ[i])


    if (len(List) > 0 and len(ob) >0):
         List = list(set(List))

#        print STAZ
#        print COMP
#        print MAXR
#        print MAXT
#        print MAXZ
#        print STDR
#        print STDT
#        print STDZ
#        print MEAR
#        print MEAT
#        print MEAZ

         listToRemove = ' '.join(['%d:%s' % (List[n],ob[List[n]*3].stats.station) for n in xrange(len(List))])
         print "\n\nStation to clean because of sig2noise: ",listToRemove,"\n\n"

         # Purge station from observed
         ob  = purgeStream(ob,List)
         if(len(ob)==0):
            print "No data left. No solution found. Exit"
            sys.exit()

    # Statistically i need at least 5 stations --> len(ob)=15
    if (len(SubList) > 0 and len(ob) > 15):
         SubList = list(set(SubList))
         listToRemove = ' '.join(['%d:%s' % (SubList[n],ob[SubList[n]*3].stats.station) for n in xrange(len(SubList))])
         print "\n\nStation to clean because of sig2noise level2: ",listToRemove,"\n\n"

         # Purge station from observed
         ob  = purgeStream(ob,SubList)
         if(len(ob)==0):
            print "No data left. No solution found. Exit"
            sys.exit()

    return (ob, List)

def cleanSpike(ob, sy, args):

    # set empty list of stations to remove because of spikes
    cleanList = []

    # set the length of the x and y array
    # X is distance in km from epicenter
    # Y is the maximal amplitude, for obs and for syn
    # M is the mean of the Y value
    X       = np.zeros(len(ob)/3)
    Y_obs_r = np.zeros(len(ob)/3)
    Y_obs_t = np.zeros(len(ob)/3)
    Y_obs_z = np.zeros(len(ob)/3)
    Y_syn_r = np.zeros(len(ob)/3)
    Y_syn_t = np.zeros(len(ob)/3)
    Y_syn_z = np.zeros(len(ob)/3)
    M_obs   = np.zeros(len(ob)/3)
    M_syn   = np.zeros(len(ob)/3)

    # now put max amplitudes into np.arrays
    for i in range(len(ob)/3):
        u = i*2
        X[i]       = float(ob[i*3+0].stats.dist) 
        Y_obs_r[i] = max(np.absolute(ob[i*3+0].data))
        Y_obs_t[i] = max(np.absolute(ob[i*3+1].data))
        Y_obs_z[i] = max(np.absolute(ob[i*3+2].data))
        Y_syn_r[i] = max(np.absolute(sy[i*3+0].data))
        Y_syn_t[i] = max(np.absolute(sy[i*3+1].data))
        Y_syn_z[i] = max(np.absolute(sy[i*3+2].data))
        M_obs[i]   = (Y_obs_r[i] + Y_obs_t[i] + Y_obs_z[i]) / 3
        M_syn[i]   = (Y_syn_r[i] + Y_syn_t[i] + Y_syn_z[i]) / 3

    # now normalize Y and M avlues to 1
    maxM_obs = max(M_obs)
    maxM_syn = max(M_syn)
    M_obs    = M_obs/maxM_obs 
    M_syn    = M_syn/maxM_syn 
  
 
    # now try fit 
    print "\n\n"
    obs_opt, obs_cov = curve_fit(func, X, M_obs)
    print "OBS ",obs_opt[0], obs_opt[1]
#   print "OBS ",obs_cov
    syn_opt, syn_cov = curve_fit(func, X,M_syn)
    print "SYN ",syn_opt[0],syn_opt[1]
#   print "SYN ",syn_cov
 
    # Now put condition of b: if b(obs)<0 and b(syn)>0, then search for station to clean

#   print "ZZZZZZZZ A Obs Syn - B Obs Syn",args.title,obs_opt[0],syn_opt[0],"  -  ",obs_opt[1] ,syn_opt[1] 
#   if   (obs_opt[1] >= 0):
#      pass

#   else:
    if (syn_opt[0] > 0 and syn_opt[1] > 0):
       if (obs_opt[1] < 0 or obs_opt[0] <0):
         # compute residuals of max amplitudes between observed and theoretical 
         res = getRes(X,M_obs,syn_opt[0],syn_opt[1],ob)
         # compute mead and std to remove outliers

         # to reject outlayers m*std thershold
         cleanList = reject_outliers(res,1)

#     yo = func(X,obs_opt[0],obs_opt[1])
#     ys = func(X,syn_opt[0],syn_opt[1])
#     plot(X,yo,color='k')
#     plot(X,ys,color='r')
#     show()
#     sys.exit()
 
    return cleanList
    
    
def getRes(x,m,a,b,ob):

    # Compute residuals of max amplitudes between observed and theoretical 
    # y: vector of residual
    y = np.zeros(len(x))
    for i in range(len(x)):
        y[i] = m[i] - func(x[i],a,b)
#       print("%d  %10.6f %10.6f %10.6f %s" % (i,y[i],m[i],func(x[i],a,b),ob[i*3+0].stats.station))
        
    return y

def reject_outliers(data, m):

    y = np.zeros(len(data))
    y = []

    mean_data = np.mean(data) 
    std_data  = np.std(data) 

    for i in range(len(data)):
       if (abs(data[i] - mean_data) > m * std_data):
          y.append(i)
#         y[i] = 1

    return y


def reject_outliers2(data, m=2):
    return data[abs(data - np.mean(data)) > m * np.std(data)]

def func(x,a,b):
    return a/(x)+b
