#!/opt/local/bin/python2.7


import os
import sys
import stat
import os.path
import argparse
from makeLibGreen import generateGreens
from readGreens import aquireGreens
from filterSeismograms import filtering

def checkConsistency(self):

    if(self.bandpass != '0'):
       li = self.bandpass.split()
       if(li[2] <= li[1]):
          print "bandpass filter: Use nrCorners min_freq max_freq"
          sys.exit()

    if self.model != "None":
       aa=os.path.exists(self.model)
       if aa == False:
          print "Model --model", self.model, "does not exists"
          sys.exit()
    else:
       print "Model file unknown. Use --model option"
       sys.exit()

def rangeToList(args):

    D    = 0
    kms  = []
    dkm  = int(float(args.dkm)) * 1
    dist = args.range.split()
    dmin = int(float(dist[0])) * 1
    dmax = int(float(dist[1])) * 1 
    D    = int(float(dist[0])) * 1
    while D <= dmax:
          kms.append(D)
          D = D + dkm

    return kms

def which(name):
    found = 0 
    out_path = 'none'
    for path in os.getenv("PATH").split(os.path.pathsep):
        full_path = path + os.sep + name
        if os.path.exists(full_path):
            """
            if os.stat(full_path).st_mode & stat.S_IXUSR:
                found = 1
                print(full_path)
            """
            found = 1
            out_path = full_path
            break

    return full_path

def doFinalLibPart(depth, npts, delta, args):

    # If args.libpath == pytdmt libraries are installed into
    # the actual runin pytdmt libarry
    # 
    mainPath = which('pytdmt.py')
    mainPath = mainPath.replace(os.sep + 'py' + os.sep + 'pytdmt.py','')
    if (args.libpath == 'pytdmt'):
       mainPath = mainPath + os.sep + 'lib'
    else:
       mainPath = mainPath + os.sep + args.libpath
        
    # take model earth
    foe = args.model.split('/')
    EModel = foe[-1]

    # create main lib if not exists
    if (os.path.exists(mainPath) == False):
       try:
         os.makedirs(mainPath)
       except:
         print "Unable to create directory " + mainPath

    # generate work temp-subpath
    if (os.path.exists(mainPath + os.sep + 'WORK') == False):
       try:
         os.makedirs(mainPath + os.sep + 'WORK')
       except:
         print "Unable to create directory " + mainPath + os.sep + 'WORK'

    # based on the greens parameters (delta, npts, dkm, depth .. etc,
    # create subdirectories
    # structure
    # Earth model (PREM)
    # filter(unfiltered/001-002)
    # depth (010)
    # npts  (02048)
    # delta 0050
    if (args.filtered != 'Y'):
       f_mod = 'unfiltered'
    else:
       fee = args.bandpass.split()
       f_mod = "%1d-%.3f-%.3f" % (int(fee[0]),float(fee[1]), float(fee[2]))
    d_mod = "%03d" % (float(depth))
    n_mod = "%05d" % (int(npts))
    D_mod = "%04d" % (float(delta) * 100)
#   p_mod = "%03d" % (int(args.pre))
    p_mod = "%03d" % (0)



    subPath = EModel + os.sep + f_mod + os.sep + d_mod + os.sep + n_mod + os.sep + p_mod + os.sep + D_mod

    print "... Library: ", mainPath + os.sep + subPath

    return mainPath, subPath
