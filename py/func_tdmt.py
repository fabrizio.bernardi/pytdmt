####################################################################
#
# Script tdmt: Apply Time Domain MT convolution/inversion
#
####################################################################

import sys,os,copy
import numpy as np
from loadData import LoadObserved
from processData import cleanStream,removeMeanTrend,cutWindow,rotateToGCP
from processData import selectData, dlazStream,getStationslist,sortStream
from processData import purgeListStation
from processData import cleanMultipleFragmentTraces,cleanZeroMaxTraces
from processData import cleanMultipleTraces
from processData import addPreSignal
from filterSeismograms import removeInstrument,filtering,decimate
from makeGreens import generateGreens
from readGreens import aquireGreens,upGrStats,reorderGreen
from makeSynt import makeSynt
from plotMTfit import plotMTfit
from invTDMT import invTDMT
from cleanSetMT import cleanSpike
from cleanSetMT import cleanNoise
from cleanSetMT import purgeStream
from cleanSetMT import getVarianceVar
from cleanSetMT import FindSpike
from myParser import applyAutoSettings
from processData import getSTationslist
from plotMaps import createKML
from plotMaps import createGMT
from loadSynt import loadGreensFromLib
from readGreens import velTodisData

def tdmt(args):

   # Begin data aquisition
   # ---------------------
   #
   # Load observed data: fseed, mseed, RESP, PAZ, Station_file
   print "... load data"
   dataStream = LoadObserved(args)
   #
   # compute distances and angles between event and station
   dataStream = dlazStream(dataStream,args)
   #
   # select data stations
   dataStream = selectData(dataStream,args) 
   if(len(dataStream)==0):
     print "no data found "
     sys.exit()
   #
   # Remove stations with multiple segment windows or less than 2 components
   dataStream = cleanMultipleFragmentTraces(dataStream,args) 
#  dataStream = cleanMultipleTraces(dataStream,args) 
   #
   #here check if empty 0 line:
   dataStream = cleanZeroMaxTraces(dataStream,args)
   # cut windwow
   (dataStream,Tb,Te) = cutWindow(dataStream,'d',args)
   if(len(dataStream)==0):
     print "no data in stream"
     sys.exit()
   #
   # clean stream from traces with gaps, incomplete trace and
   # stations with 1 or 2 components only, or trace of zeros
   dataStream = cleanStream(dataStream,Tb,Te,args)
   if(len(dataStream)==0):
     print "Data pool empty. Exit."
     sys.exit()
   #
   # sort stream for station/distance/azimuth
   dataStream = sortStream(dataStream,args)
   #
   # remove trend and mean
   dataStream = removeMeanTrend(dataStream)
   #
   # End data aquisition

   # Begin removing instrument and filtering
   # ---------------------------------------
   #
   if(args.deco == "Y"):
     print "... removing instrument" 
     dataStream = removeInstrument(dataStream,args)
   # From m to cm
   if(args.m2cm == "Y"):
     for i in range(len(dataStream)):
         dataStream[i].data *= 100 

   # here check if empty 0 line: this maybe because of zero line and/or wron PZ file
   dataStream = cleanZeroMaxTraces(dataStream,args)
   #
   print "... rotate to GCP, filtering and decimation"
   datastream = rotateToGCP(dataStream)
   #
#  dataOrigin = copy.deepcopy(dataStream)
   dataStream = filtering(dataStream,'d',args)
   #
   dataStream = decimate(dataStream,'d',args)
   #
   (dataStream, noiseList) = cleanNoise(dataStream, args)
#  for n in range(len(dataStream)):
#      dataStream[n].write(dataStream[n].stats.station + '.' + dataStream[n].stats.channel + '.' + dataStream[n].stats.location + '.sac', format='SAC')
   #
   # check if dataSteram empty before to continue
   if(len(dataStream)==0):
     print "\n !! Empty dataStrem after cleaning !!"
     sys.exit()

   # Check if too many stations
   StList = getStationslist(dataStream)
   stationToPurge = []
   listToPurge    = []
   if(len(StList)>100):
       for l in range(100,len(StList)):
         listToPurge.append(l)
         stationToPurge.append(StList[l])
       print "QQ 1"
       dataStream  = purgeStream(dataStream,listToPurge)
       print "Only 100 stations allowed (FKRPROG!!). Take only first 100 station of the sorted stream.!"
       print "  Stations removed: ", stationToPurge,"\n"

#  for i in range(len(dataStream)):
#      dataStream[i].stats.az = 180 - dataStream[i].stats.az

   if(int(args.dva) == 2):
     for i in range(len(dataStream)):
       dataStream[i].data = np.diff(dataStream[i].data)

   dataOrigin = copy.deepcopy(dataStream)

     
     
   # Begin Greensfunction
   # --------------------
   # generate earth model
   #
   if(args.lib == "N"):
      print "... generate greens"
      greenSpecFile = generateGreens(dataStream,args)
      #
      # greens (spectral domain) -> greenStream (time domain)
      print "... spectral to time domain"
      greenStream   = aquireGreens(greenSpecFile,args) 
    
      # update stats.station of greenStream
      print "... update stream header and sync with data"
      greenStream   = upGrStats(greenStream,dataStream)
      #
      # make rigth order of greens elements
      StazList = getStationslist(greenStream)
      greenStream = reorderGreen(greenStream,StazList)

   else:
      greenStream = loadGreensFromLib(dataStream,args)

      greenStream   = upGrStats(greenStream,dataStream)

      greenStream   = addPreSignal(greenStream, args)

      if (args.libType == 'NM'):
         for i in range(len(greenStream)):
             greenStream[i].stats.channel = greenStream[i].stats.channel.lower()
             if(greenStream[i].stats.channel == 'tss' or \
                greenStream[i].stats.channel == 'xds' or \
                greenStream[i].stats.channel == 'zss' or \
                greenStream[i].stats.channel == 'zdd'):
                  greenStream[i].data = greenStream[i].data *(-1)  

             if(int(args.dva) == 1):
                greenStream[i].data = velTodisData(greenStream[i].data, float(greenStream[i].stats.delta))
             
   #
   # filtering
   print "... greens filtering"
   GreenOrigin = copy.deepcopy(greenStream)
   greenStream = filtering(greenStream,'g',args)
   greenStream = decimate(greenStream,'g',args)
   
   ################################################################
   # ---- MAKE TDMT
   # Run first MT RUN 
   print "\n... RUN TDMT\n"
   if(args.nrIter == '0'):
     print "Status before realignment: "
     (greenStream, dataStream, synStream, MTx, metaMT, spin, rmStaz) = invTDMT(greenStream,dataStream,args, 0)
     print "\nStatus after realignment: "
     (greenStream, dataStream, synStream, MTx, metaMT, spin, rmStaz) = invTDMT(greenStream,dataStream,args, 0)
   else:
     (greenStream, dataStream, synStream, MTx, metaMT, spin, rmStaz) = invTDMT(greenStream,dataStream,args, 0)
   #
   ################################################################

   ################################################################
   # BEGIN CLEAN SPIKES
   # Clean dataset from spikes
   if (args.clean == "1" and len(dataStream) <3):
      print "Spike detection not possible, stations not enougth"

   elif(args.clean == "1" and len(dataStream) >=3):
      print "\n\nRun spike detection and clean\n"
   else:
      pass

   while (args.clean == "1" and len(dataStream) >=5):

      # select list of normalized variances
      varianceVec = getVarianceVar(dataStream)

      # find spikes if exists
      Spikes, normSpike, SpikeValue, spikeStatus  = FindSpike(varianceVec)

      # remove spike if exists
      if(spikeStatus == True):

         listToRemove = ' '.join(['%d:%s' % (Spikes[n],dataStream[Spikes[n]*3].stats.station) for n in xrange(len(Spikes))])
         print  "Station to clean because of spike: ",listToRemove, normSpike, SpikeValue

         # Purge station from observed
         dataStream  = purgeStream(dataStream,Spikes)
         if(len(dataStream)==0):
            print args.title," No data left. No solution found. Exit"
            sys.exit()
         # Purge station from greens
         greenStream = purgeStream(greenStream,Spikes)

         # re-inv
         (greenStream, dataStream, synStream, MTx, metaMT, spin, rmStaz) = invTDMT(greenStream,dataStream,args, 0)
         new_varianceVec = getVarianceVar(dataStream)

      else:
         args.clean = "0"
   #
   # END CLEAN SPIKES
   ################################################################

   ################################################################
   # BEGIN ITERATIONS
   if(args.nrIter != "0"):
     i=1
     while(spin==1):


        # ---------------------------------#
        # BEGIN SYNC FOR AUTOMATIC SETTINGS
        # Apply automatic settings. resync greens and data sets as after first iteration and Spike detection if applied
        # Apply only at first iteration !!!!!
        if(args.auto == "Y" and i ==1):

           print "\n\nApply Automatic settings"
           # APPLY NEW AUTOMATIC MW-DEPENDENT SETTINGS
           args = applyAutoSettings(args, metaMT)
       
           # BEGIN SYNC data and synt: sync the data and the green as original data set
           # DATA
           dataStream = copy.deepcopy(dataOrigin)          
           # remove noisy list removed up
           if(len(noiseList)>0):
              dataStream  = purgeStream(dataStream,noiseList)
              if (len(listToPurge) >0 ):
                 dataStream  = purgeStream(dataStream,listToPurge)
           # SYN
           greenStream = copy.deepcopy(GreenOrigin)
           #
           # Remove Spikes if the case
           if(len(SpikeList)>0):
             dataStream  = purgeStream(dataStream,SpikeList)
             greenStream = purgeStream(greenStream,SpikeList)
           #
           #
           # Now select station range
           dataStream  = purgeListStation(dataStream,args,'d')
           greenStream = purgeListStation(greenStream,args,'d')
           # Maybe with new distance range, no more data
           if(len(dataStream)==0):
             print args.title," No data left with new distance range. Exit"
             sys.exit()
           #
           # check if Zcor values exceed max and min values, then remove list
           dataStream  = purgeListStation(dataStream,args,'z')
           greenStream = purgeListStation(greenStream,args,'z')
           #
           # Now filtering and decimation
           # DATA
           dataStream = filtering(dataStream,'d',args)
           dataStream = decimate(dataStream,'d',args)
           # SYN
           greenStream = filtering(greenStream,'g',args)
           greenStream = decimate(greenStream,'g',args)
           #
           # Apply first iteration MT
           # Run MT inv
           print "\n\nIteration Nr: 0"
           (greenStream, dataStream, synStream, MTx, metaMT, spin, rmStaz) = invTDMT(greenStream,dataStream,args, 0)
        # END SYNC FOR AUTOMATIC SETTINGS           
        # ---------------------------------#
        
        print "\n\nIteration Nr: ",i
        # --------------------------
        # Output stations to remove from dataset
        listToRemove = ' '.join(['%d:%s' % (rmStaz[n],dataStream[rmStaz[n]*3].stats.station) for n in xrange(len(rmStaz))])
        print "Remove Station:",listToRemove

        # Purge station from observed
        dataStream  = purgeStream(dataStream,rmStaz)
        greenStream = purgeStream(greenStream,rmStaz)
        if(len(dataStream)==0):
          print args.title,"No data left. No solution found. Exit"
          sys.exit()
        # Purge station from greens
        #
        # Run MT inv
        (greenStream, dataStream, synStream, MTx, metaMT, spin, rmStaz) = invTDMT(greenStream,dataStream,args, i)

        # check if stop
        if(i>=int(args.nrIter) and args.nrIter != '-1'):
           spin = 0
        i=i+1

   ################################################################
   # ---- Plot
   print "\n\n",args.title," Solution OK"
   print "\n... PLOT MT fit"
   print "... cut window and realign for Zcor"
   synStream  = upGrStats(synStream,dataStream)
   #
   (synStream,Tb,Te) = cutWindow(synStream,'s',args)

   # Make plot
   FinalLog = plotMTfit(dataStream,synStream,metaMT,Tb,Te,args)
   #
   #Make plot Maps
#  print "Plot maps"
   mapKML = createKML(dataStream, args.datadir)
   mapGMT = createGMT(dataStream, metaMT, args)


   # Plot observed and synthetics in sac binary format
   if(args.wsac == "Y"):
     for i in range(len(synthStream)):
        name2 = args.datadir + os.sep + synStream[i].stats.station + "." + synStream[i].stats.channel + ".sac"
        synStream[i].write(name2, format="SAC")
     for i in range(len(dataStream)):
        name2 = args.datadir + os.sep + dataStream[i].stats.station + "." + dataStream[i].stats.channel + ".sac"
        dataStream[i].write(name2, format="SAC")


