#!/usr/local/anaconda2/bin/python2.7 
# encoding: utf-8

import os
import sys
import glob
import math
import shutil
import bisect
import subprocess
import numpy as np
import matplotlib.pyplot as plt
from myParser import parseMyLine
from matplotlib.backends.backend_pdf import PdfPages
from obspy.imaging.mopad_wrapper import Beach
from matplotlib import patches, collections, transforms, path as mplpath
from obspy.core import compatibility
from time import gmtime, strftime
from datetime import datetime
import warnings

def loadStationList2(stFiles):

    
    out = np.zeros(len(stFiles))

    for i in range(len(stFiles)):
        nr = 0
        if (os.path.exists(stFiles[i]) == True):
            f = open(stFiles[i], 'r')
            for line in f:
              nr = nr + 1
            out[i] = nr
        else:
            nr = 0

    return out
       
def loadStationList(inf):

    station_list   = []
    latitude_list  = []
    longitude_list = []
    variance_list  = []
    delta_list     = []
    azimuth_list   = []
    Z_cor_list     = []
    E_cor_list     = []
    N_cor_list     = []
    station_file = inf 
    f = open(station_file, 'r')
    for line in f:
        foe = line.split()
        station_list.append(str(foe[0]))
        latitude_list.append(str(foe[1]))
        longitude_list.append(str(foe[2]))

    return station_list, latitude_list, longitude_list

def findBestFit(args, depths):

    dd = "%03d" % (depths)

    lista = glob.glob(args.invdir + os.sep + args.pltname + '*' + dd + '*.pdf')

    out = []

    for i in range(len(lista)):
        d = lista[i].split(os.sep)
        out.append(d[-1])

    return out
   
def depths_2_mtFit(dept):

    mt = []
    for i in range(len(dept)):
        dep = "%03d" % dept[i]
        mt.append(args.pltname + '_mt_z.' + dep + "_")

    return mt

def depths_2_mtOut(dept):

    mt = []
    for i in range(len(dept)):
        dep = "%03d" % dept[i]
        mt.append(args.invdir + os.sep + 'mt_z.' + dep + '.out')

    return mt

def defNewEpi(epi, dept):

    
    epi=epi.translate(None, '\"')
    tmp = epi.rstrip().split()
    tmp[-1] = str(dept)
    tmp = (' ').join(tmp)

    return tmp

def getVars(mt_outs):

    dep = np.zeros(len(mt_outs))
    var = np.zeros(len(mt_outs))
    pdc = np.zeros(len(mt_outs))
    stk = np.zeros(len(mt_outs))
    dip = np.zeros(len(mt_outs))
    slp = np.zeros(len(mt_outs))
    Mxx = np.zeros(len(mt_outs))
    Myy = np.zeros(len(mt_outs)) 
    Mzz = np.zeros(len(mt_outs))
    Mxy = np.zeros(len(mt_outs)) 
    Mxz = np.zeros(len(mt_outs)) 
    Myz = np.zeros(len(mt_outs))
    Mw  = np.zeros(len(mt_outs))
    NrS = np.zeros(len(mt_outs))
    
    title  = 'No solution found'

    nam = []

    for i in range(len(mt_outs)):
        try:
           f = open(mt_outs[i], 'r')
           lines=f.readlines()
           title    = lines[0].rstrip()
           info_org = lines[1].rstrip().split()
           info_mt1 = lines[2].rstrip().split()
           info_mt2 = lines[3].rstrip().split()
           info_pln = lines[4].rstrip().split()
           info_dpc = lines[8].rstrip().split()
           info_var = lines[9].rstrip().split()
           nrStaz   = lines[10].rstrip().split()

           nam.append(mt_outs[i])
           NrS[i] = int(nrStaz[2])
           dep[i] = float(info_org[6])
           Mxx[i] = float(info_mt1[2])
           Myy[i] = float(info_mt1[5])
           Mzz[i] = float(info_mt1[8])
           Mxy[i] = float(info_mt2[2])
           Mxz[i] = float(info_mt2[5])
           Myz[i] = float(info_mt2[8])
           stk[i] = float(info_pln[2])
           dip[i] = float(info_pln[4])
           slp[i] = float(info_pln[6])
           var[i] = float(info_var[4])
           pdc[i] = float(info_dpc[6])
           Mw[i]  = float(info_dpc[4])
        except:
           nam.append(mt_outs[i])
        
    return nam, dep, var, pdc, Mxx, Myy, Mzz, Mxy, Mxz, Myz, stk, dip, slp, Mw, title, NrS
#   return nam, var, pdc, Mxx, Myy, Mzz, Mxy, Mxz, Myz, stk, dip, slp, Mw, title, int(nrStaz[2])

def buildNewCommand(pytdmt, list_arguments, depths, mtfit, nr, stations):

    for i in range(len(list_arguments)):

        if(list_arguments[i] == "--pltname"): 
           list_arguments[i+1] = mtfit[nr]
    
        if(list_arguments[i] == "--epi"):
           list_arguments[i+1] = defNewEpi(list_arguments[i+1], depths)
           list_arguments[i+1] = "\"" + list_arguments[i+1] + "\""

        if(list_arguments[i] == "--nrIter"):
#          if(nr == 0 or stations == "none"):
           if(stations == "none"):
              list_arguments[i+1] = "-1"
           else: 
              list_arguments[i+1] = "0"

        if(list_arguments[i] == "--clean"):
#          if(nr == 0 or stations == "none"):
           if(stations == "none"):
              list_arguments[i+1] = "1"
           else: 
              list_arguments[i+1] = "0"

        if(list_arguments[i] == "--plt"):
           list_arguments[i+1] = "N"

        if(list_arguments[i] == "--title" or list_arguments[i] == "--bandpass" or \
               list_arguments[i] == "--highpass" or list_arguments[i] == "--lowpass" or \
               list_arguments[i] == "--sort" or list_arguments[i] == "--range" or \
               list_arguments[i] == "--purge" or list_arguments[i] == "--set" or \
               list_arguments[i] == "--azi" or list_arguments[i] == "--flim"):

                 list_arguments[i+1] = list_arguments[i+1].translate(None, '\"')
                 list_arguments[i+1] = "\"" + list_arguments[i+1] + "\""

    if(nr == 0 and stations != "none"):
        list_arguments.append("--set")
        list_arguments.append("\"" + stations + "\"")
   
    list_arguments = ' '.join(list_arguments)

    command = pytdmt + " " + list_arguments

    return command 

def getCMT(inf):

    cmt_dep = 0
    Mw      = 0
    np1     = [0, 0, 0]
    mtt     = [0, 0, 0, 0, 0, 0]

    if (os.path.exists(inf) == True):
        f = open(inf, 'r')
        lines=f.readlines()

        #depth
        cmt_dep = float(lines[0].rstrip())

        # mtelements
        cmt_mki = lines[2].rstrip().split()
        mtt     = np.zeros(len(cmt_mki))
        mtt     = [float(cmt_mki[0]), float(cmt_mki[1]), float(cmt_mki[2]), \
                   float(cmt_mki[3]), float(cmt_mki[4]), float(cmt_mki[5])]

        # magnitude
        cmt_mag = lines[3].rstrip().split()
        Mo      = float(cmt_mag[6])
        Mw      = "%.2f" % (math.log10(Mo)/1.5-10.73)

        # Plan1
        cmt_pla = lines[4].rstrip().replace('=',' ').split()
        np1     = list([float(cmt_pla[3]), float(cmt_pla[5]), float(cmt_pla[7])])

        #beach1 = Beach(mtt, xy=(-70, 80), width=30)

    return cmt_dep, Mw, np1, mtt, 103

def getCMTfrom_ndk(inf):

    mt_dep = 0
    mt_mw  = 0
    mt_np1 = [0, 0, 0]
    mt_mtt = [0, 0, 0, 0, 0, 0]

    if (os.path.exists(inf) == True):
        f = open(inf, 'r')
        lines=f.readlines()

        # depth
        ifoe    = lines[0].rstrip()
        mt_dep = float(ifoe[42:47]) 

        # Mw
        ifoe    = lines[3].rstrip()
        exp     = str(ifoe[0:2]) 
        ifoe    = lines[4].rstrip()
        sca     = str(ifoe[49:56])
        mo      = float(sca + 'e' + exp)
        mt_mw   = "%.2f" % (math.log10(mo)/1.5-10.73)

        # Plan1
        ifoe    = lines[4].rstrip()
        npt     = ifoe[58:80].split()
        mt_np1 = list([float(npt[0]), float(npt[1]), float(npt[2])])

        # Mt elemnts
        ifoe    = lines[3].rstrip()
        mij     = ifoe[2:80].split()
        mt_mtt = [float(mij[0]), float(mij[2]), float(mij[4]), float(mij[6]), float(mij[8]), float(mij[10])]

    return mt_dep, mt_mw, mt_np1, mt_mtt, 103

def plotSolutions(files, var, pdc, Mxx, Myy, Mzz, Mxy, Mxz, Myz, stk, dip, slp, Mw, title, Quality, bestZ, indexBest, args):


    if(args.gcmt != 'None'):
       cmt_depth, cmt_Mw, cmt_np1, cmt_mtt, cmt_var = getCMTfrom_ndk(args.gcmt)


    fig=plt.figure(1,figsize=(11.69, 8.27),facecolor='w')
    ax = fig.add_subplot(1, 2, 1)
    ax.set_transform(transforms.IdentityTransform())
    ax.set_xlim((0, 55))
    ax.set_ylim((30, 105))
    plt.ylabel("Normalized Variance")
    plt.xlabel("Source Depth [km]")

    if(Quality == "A"):
      color_best = 'green'
    elif (Quality == "B"):
      color_best = 'yellow'
    else:
      color_best = 'r'
    bestMag = 0

    for i in range(len(depths_test)):

        if(depths_test[i] < 55 and var[i] >= 30):
           np1 = [stk[i], dip[i], slp[i]]
           mtx = [Mzz[i], Mxx[i], Myy[i], Mxz[i], -1*Myz[i], -1*Mxy[i]]

           if (i==indexBest):
               beach1 = Beach(np1, xy=(depths_test[i], var[i]), \
                     linewidth=0.5, width=3.8, nofill=True, zorder=100, alpha=1.00, facecolor=color_best)
               beach2 = Beach(mtx, xy=(depths_test[i], var[i]), \
                     linewidth=1.0, width=3.8, nofill=False,  zorder=1, facecolor=color_best)
               # position magnitude:
               XPos = depths_test[i] - 1.3
               YPos = var[i] + 2.5
               ax.text(XPos, YPos, Mw[i], fontsize=10) #99FFFF
               bestMag = Mw[i]
           else:
               beach1 = Beach(np1, xy=(depths_test[i], var[i]), \
                     linewidth=0.5, width=3.0, nofill=True, zorder=100, alpha=1.00, facecolor='gray')
               beach2 = Beach(mtx, xy=(depths_test[i], var[i]), \
                     linewidth=1.0, width=3.0, nofill=False,  zorder=1, facecolor='gray')
               # position magnitude:
               XPos = depths_test[i] - 1.3
               YPos = var[i] + 2
               ax.text(XPos, YPos, Mw[i], fontsize=8) #99FFFF

           ax.add_collection(beach1)
           ax.add_collection(beach2)

    # Second plot
    ax1 = fig.add_subplot(1, 2, 2)
    ax1.set_xlim((50, 650))
    ax1.set_transform(transforms.IdentityTransform())
    ax1.set_ylim((30, 105))
    plt.xlabel("Source Depth [km]")
    ax2 = ax1.twiny()
    ax2.set_xlim(5, 65)
    ax2.set_axis_off()
    for i in range(len(depths_test)):

        if(depths_test[i] >= 55 and var[i] >= 30):
           np1 = [stk[i], dip[i], slp[i]]
           mtx = [Mzz[i], Mxx[i], Myy[i], Mxz[i], -1*Myz[i], -1*Mxy[i]]

           if (i==indexBest):
               beach2 = Beach(mtx, xy=(depths_test[i]/10, var[i]), \
                     linewidth=1.0, width=3.8, nofill=False,  zorder=1, facecolor=color_best)
               beach1 = Beach(np1, xy=(depths_test[i]/10, var[i]), \
                     linewidth=0.5, width=3.8, nofill=True, zorder=100, alpha=1.00, facecolor=color_best)
               # position magnitude:
               XPos = depths_test[i] - 0.0
               YPos = var[i] + 2.5
               ax2.text(XPos/10-2.1, YPos, Mw[i], fontsize=10) #99FFFF
               bestMag = Mw[i]
           else:
               beach2 = Beach(mtx, xy=(depths_test[i]/10, var[i]), \
                     linewidth=1.0, width=3.0, nofill=False,  zorder=1, facecolor='gray')
               beach1 = Beach(np1, xy=(depths_test[i]/10, var[i]), \
                     linewidth=0.5, width=3.0, nofill=True, zorder=100, alpha=1.00, facecolor='gray')
               # position magnitude:
               XPos = depths_test[i] - 15.3
               YPos = var[i] + 2
               ax2.text(XPos/10, YPos, Mw[i], fontsize=8) #99FFFF

           ax2.add_collection(beach1)
           ax2.add_collection(beach2)

    

    if(args.gcmt != 'None'):
       if(cmt_depth < 55 and cmt_depth > 0):
          XCMT = cmt_depth + 1.0
          YCMT = cmt_var + 0
          beach4 = Beach(cmt_np1, xy=(cmt_depth, cmt_var), \
                   linewidth=0.5, width=3.5, nofill=True, zorder=100, alpha=1.00, facecolor='r')
          beach5 = Beach(cmt_mtt, xy=(cmt_depth, cmt_var), \
                   linewidth=1.0, width=3.5, nofill=False,  zorder=1, facecolor='r')
          ax.text(XCMT+2.2, YCMT, cmt_Mw, fontsize=10)
          ax.add_collection(beach4)
          ax.add_collection(beach5)
       if(cmt_depth >= 55):
          XCMT = cmt_depth - 1.3
          YCMT = cmt_var - 0
          beach4 = Beach(cmt_np1, xy=(cmt_depth/10, cmt_var), \
                   linewidth=0.5, width=3.5, nofill=True, zorder=100, alpha=1.00, facecolor='r')
          beach5 = Beach(cmt_mtt, xy=(cmt_depth/10, cmt_var), \
                   linewidth=1.0, width=3.5, nofill=False,  zorder=1, facecolor='r')
          ax2.text(XCMT/10+2.2, YCMT, cmt_Mw, fontsize=10)
          ax2.add_collection(beach4)
          ax2.add_collection(beach5)


    bestDepth = depths_test[indexBest]
    bestMag   = Mw[indexBest]
    title     = title + "\nBest depth : " + str(bestZ) + "[km]    Mw = " + str(bestMag)
    plt.suptitle(title, fontsize=14)
    fig.savefig(args.invdir + os.sep + 'variance-depth.pdf')
#   plt.show()
  
    return True

def findall(L, test):
    i=0
    indices = []
    while(True):
        try:
            # next value in list passing the test
            nextvalue = filter(test, L[i:])[0]

            # add index of this value in the index list,
            # by searching the value in L[i:] 
            indices.append(L.index(nextvalue, i))

            # iterate i, that is the next index from where to search
            i=indices[-1]+1
        #when there is no further "good value", filter returns [],
        # hence there is an out of range exeption
        except IndexError:
            return indices

def SetFirstIterationDepth(depths, Zs, nr):

    if(nr != len(Zs) - 0):

       ind = findall(depths, lambda x:x==Zs[nr])

       Z = depths.pop(ind[0])

       depths = [Z] + depths

    else:
       pass

    return depths


def setQuality(NrSt, var):

    quality = "C"
    paramet = "(Unreliable solution)"

    if(NrSt == 1 and var >= 70):
      quality = "B"
      paramet = "(Reliable magnitude Mw)"
    if(NrSt >= 2 and NrSt <= 3 and var >= 50):
      quality = "B"
      paramet = "(Reliable magnitude Mw)"
    if(NrSt > 3 and NrSt <= 4 and var >= 30 and var < 50):
      quality = "B"
      paramet = "(Reliable magnitude Mw)"
    if(NrSt >= 3 and var >= 50):
      quality = "A"
      paramet = "(Reliable focal mechanism and magnitude Mw)"

    return quality, paramet

def setDepthUnc(depths, var, index, unc):

    min_var = var[index] - var[index] * unc 

    unc_range = [depths[0], depths[-1]]
    unc_index = [0,0]

    best_var = var[index]

    #left side:
    spin = 0
    i = index
    if (index != 0):
       for i in range(1, index):
           if (min_var <= var[i] and min_var >= var[i-1]):
               unc_range[0] = depths[i]
               unc_index[0] = i
               break
    if (index != len(var) - 1):
       for i in range(index, len(var) - 2):
           if (min_var <= var[i] and min_var >= var[i+1]):
               unc_range[1] = depths[i]
               unc_index[1] = i
               break
 
    
    return unc_range, unc_index 

def point_inside_polygon(x,y,poly):
    """
    determine if a point is inside a given polygon or not
    Polygon is a list of (x,y) pairs.

    x: float (longitude)
    y: float (latitude)
    poly: list of float pairs (polygon)

    Example: x = -11.2; y = 36.1
             poly = ([-9,28], [-9,39], [4,44], [13,49], [40,49], [40,28]) 
    """

    n = len(poly)
    inside = False

    p1x,p1y = poly[0]
    for i in range(n+1):
        p2x,p2y = poly[i % n]
        if y > min(p1y,p2y):
            if y <= max(p1y,p2y):
                if x <= max(p1x,p2x):
                    if p1y != p2y:
                        xinters = (y-p1y)*(p2x-p1x)/(p2y-p1y)+p1x
                    if p1x == p2x or x <= xinters:
                        inside = not inside
        p1x,p1y = p2x,p2y

    return inside


def defineZtests(lat, lon):

    area     = "Area default"

    in_area1 = False
    in_area2 = False

    F_test0  = [18, 75]
    Z_test0  = [6, 10, 14, 18, 22, 26, 30, 41, 50, 75, 100]
#   F_test0  = [18, 75, 151]
#   Z_test0  = [6, 10, 14, 18, 22, 26, 30, 41, 50, 75, 100, 151]


    F_test1  = [18, 75, 200, 401]  
    Z_test1  = [6, 10, 14, 18, 22, 26, 30, 41, 50, 75, 100, 151, 200, 300, 401, 501, 601]  

    F_test2  = [18, 75, 200]  
    Z_test2  = [6, 10, 14, 18, 22, 26, 30, 41, 50, 75, 100, 151, 200, 300, 401]  

    # very deep [0-600]
    lats  = [36.00,36.00,37.75,37.75, 36.00]
    lons  = [-4.50, -2.25, -2.25, -4.50, -4.50]
    area1 = []
    for i in range(len(lats)):
        area1.append([lats[i], lons[i]])
        in_area1 = point_inside_polygon(lat, lon, area1)
 
    
    # Italy deep: [0-500] (200-500)
    lats  = [37.75,37.75,40.50,41.75,41.75,37.75]
    lons  = [12.75,16.00,16.00,13.50,12.75,12.75]
    area2 = [] 
    for i in range(len(lats)):
        area2.append([lats[i], lons[i]])
        in_area2 = point_inside_polygon(lat, lon, area2)

    if  (in_area1 == True):
        area = "Area 1"
        return F_test1, Z_test1, area
       

    elif(in_area2 == True):
        area = "Area 2"
        return F_test2, Z_test2, area
   
    else:
        return F_test0, Z_test0, area


##################################################################################################
#BEGIN
begT = strftime("%Y-%m-%d %H:%M:%S", gmtime())
iniT = datetime.now() 
print "\n\n================================"
print "BEGIN AUTOMATIC PYTDMT PROCEDURE"
print "--------------------------------"
print "Begin Time:", begT
print "================================"

args=parseMyLine()
list_arguments = sys.argv[1:]
if not list_arguments:
       print "Run this application to run pytdmt for automatic best depth inversion"
       print "This script does not have a check of the arguments consistency. Please check "
       print "that the arguments are selected for propelly pytdmt runs.\n"
       print "Depths sampling inversions are hardcoded into this script. Modify vector "
       print "\"depths_test\" if you want to sample different depth. Please be carefull "
       print "that if you use synthetics libraries, libreries must exists for all tested depth." 
       sys.exit(0)

Coord             = args.epi.split()
Lat_epi           = "%.3f" % (float(Coord[0]))
Lon_epi           = "%.3f" % (float(Coord[1]))

if(float(args.fix_depth) == 0.):
   First_Z_test, depths_test, area_test = defineZtests(float(Coord[0]), float(Coord[1]))
else:
   First_Z_test, depths_test, area_test = defineZtests(float(Coord[0]), float(Coord[1]))
   First_Z_test      = [float(args.fix_depth)]
   depths_test       = [float(args.fix_depth)]
print "Defined depths test for:", area_test
print "First Z test           :", First_Z_test
print "Fine Z test            :", depths_test

Nr_Staz_test      = np.zeros(len(First_Z_test))   
Var_test          = np.zeros(len(First_Z_test))   
spin_test         = 0
position          = 0  #position in Nr_Staz_test vector
min_Staz_Required = 4

#define mt_z_xxx.out files to load 
mt_outs = depths_2_mtOut(depths_test)
mt_fits = depths_2_mtFit(depths_test)

# define pytdmet path
#pytdmt = "/Users/fabrizio/farsi/pytdmtV2.6.4/py/pytdmt.py" 
pytdmt = os.environ['PYTDMT_PY'] + os.sep + 'pytdmt.py'

if (os.path.exists(args.invdir) == False):
   os.makedirs(args.invdir)
    

# define log file for pytdmet parameters
pytdmetlog = args.invdir + os.sep + 'pytdmt_procedure.log'
pytdmetstd = args.invdir + os.sep + 'pytdmt_procedure.std'
pytdmeterr = args.invdir + os.sep + 'pytdmt_procedure.errlog'
if (os.path.exists('logs')):
   pass
else:
   os.makedirs('logs')


# clean mt_inv.log if exists
mtlog = args.invdir + os.sep + 'mt_inv.log'
mtout = args.invdir + os.sep + 'mt_inv.out'
if (os.path.exists(mtlog) == True):
   try:
        os.remove(mtlog)
   except:
        print "\n\nWARNING!!! Can't remove file ", mtlog, "\n\n\n"
        pass
if (os.path.exists(pytdmetstd) == True):
   try:
        os.remove(pytdmetstd)
   except:
        pass


if (os.path.exists(pytdmetlog) == True):
   try:
        os.remove(pytdmetlog)
   except:
        pass
   
fl = open(pytdmetlog, 'w')
ff = open(pytdmetstd, 'w')
fe = open(pytdmeterr, 'w')

####### START PROCEDURE ########
listaSta = []
listaOut = []
listaFSZ = []

i = 0
while i < len(First_Z_test):

     z = "%03d" % First_Z_test[i]
     StdOut = args.invdir + os.sep + 'mt_inv.out'
#    newNameOut = args.invdir + os.sep + 'mt_First_' + z + '.out'
     newNameOut = args.invdir + os.sep + 'FirstTrialZ_mt_' + z + '.out'
     StdOut = args.invdir + os.sep + 'mt_inv.out'
     staNameUse = args.invdir + os.sep + 'used_stations.txt'
     staNameLis = args.invdir + os.sep + 'FirstTrialStaz_' + z + '.txt'

     # in order to remove bug when only one initial depth is void, 
     # create empyFile FirstTrialZ_mt_XXX.out
     open(newNameOut, 'a').close()

     listaFSZ.append(newNameOut)

     myCommand = buildNewCommand(pytdmt, list_arguments, First_Z_test[i], mt_fits, i, 'none')
     print "\n\n"
     print "--------------------------------"
     print myCommand
     ff.write(myCommand + "\n\n")

     print "Running preliminary depth", First_Z_test[i]
     # INV1
     proc = subprocess.Popen(myCommand, stdout=subprocess.PIPE, shell=True)
     (out, err) = proc.communicate()
     fl.write(out)
     try:
       fe.write(err)
     except:
       pass
     
#    This is usless since subprocess.Popen
#    os.system(myCommand)   

     if (os.path.exists(StdOut) == True):
        try:
           shutil.move(StdOut, newNameOut)
        except:
           print "\n\nWARNING!!! Can't move file ", StdOut, "\n\n\n"
           break
        try:
           shutil.move(staNameUse, staNameLis)
        except:
           if (os.path.exists(staNameLis) == True):
               os.remove(staNameLis)  
           open(staNameLis, 'a').close()
           

     listaOut.append(newNameOut)
     listaSta.append(staNameLis)

     i = i + 1       

files, depth, var, pdc, Mxx, Myy, Mzz, Mxy, Mxz, Myz, stk, dip, slp, Mw, title, NrSt = getVars(listaFSZ)

if(np.mean(depth) != 0):
   nrSta = loadStationList2(listaSta)
   FirstDepthIndex = np.argmax(nrSta)
   stations_list, latitude_list, longitude_list = loadStationList(listaSta[FirstDepthIndex])
   stations_string = ' '.join(stations_list)

   if(np.mean(nrSta) == 0 and np.std(nrSta) == 0):
     print "\n\n=============================="
     print "END AUTOMATIC PYTDMT PROCEDURE"
     print "NO SOLUTION FOUND FOR ALL INITIAL TEST DEPTHS:", First_Z_test, "EXIT!"
     print "==============================\n"
#    print "ciao2"
     sys.exit()
else:
   print "\n\n=============================="
   print "END AUTOMATIC PYTDMT PROCEDURE"
   print "NO SOLUTION FOUND FOR ALL INITIAL TEST DEPTHS:", First_Z_test, "EXIT!"
   print "==============================\n"
#  print "ciao3"
   sys.exit()

if(float(args.fix_depth) == 0):
   print "\n\n"
   print "PRELIMINARY SOLUTION"
   print "--------------------"
   print "Station -> Depth:", nrSta, First_Z_test, "  --> Best Preliminary Depth at:", First_Z_test[FirstDepthIndex], "[km]"
   print "Station List to use:", stations_string 
   f = open(files[FirstDepthIndex], 'r')
   for lines in f:
       print lines.rstrip()
   print "\n"

dd = "%03d" % (First_Z_test[FirstDepthIndex])
shutil.copy2(files[FirstDepthIndex], args.invdir + os.sep + 'preliminary_mt_' + dd + '.out')

if(float(args.fix_depth) == 0):

  # Now restart with all trial depth using the station list
  print "Start Refining Solution"
  print "-----------------------"

  i = 0
  listaOut = []
  while i < len(depths_test):


     z = "%03d" % depths_test[i]
     StdOut = args.invdir + os.sep + 'mt_inv.out'
     newNameOut = args.invdir + os.sep + 'mt_' + z + '.out'

     listaOut.append(newNameOut)

     myCommand = buildNewCommand(pytdmt, list_arguments, depths_test[i], mt_fits, i, stations_string)
#    print myCommand
     ff.write(myCommand + "\n\n")

     # INV2
     print "Running depth", depths_test[i]
     proc = subprocess.Popen(myCommand, stdout=subprocess.PIPE, shell=True)
     (out, err) = proc.communicate()
     fl.write(out)
     try:
       fe.write(err)
     except:
       pass

     if (os.path.exists(StdOut) == True):
        try:
           shutil.move(StdOut, newNameOut)
        except:
           print "\n\nWARNING!!! Can't move file ", StdOut, "\n\n\n"
           break


     i = i + 1

else:
  listaOut = [args.invdir + os.sep + 'preliminary_mt_' + dd + '.out']


files, depth, var, pdc, Mxx, Myy, Mzz, Mxy, Mxz, Myz, stk, dip, slp, Mw, title, NrSt = getVars(listaOut)

FirstDepthIndex = np.argmax(var)
bestDepth = depth[FirstDepthIndex]
#print "------> ", depth[FirstDepthIndex], var[FirstDepthIndex]

####### END PROCEDURE ########

# Set Depth and uncertainty depth range
DUnc = 0.1
Depth_uncertainty, UncIndex = setDepthUnc(depth, var, FirstDepthIndex, DUnc)

# Set quality of solution: A, B, C 
Quality, readme = setQuality(int(NrSt[FirstDepthIndex]), float(var[FirstDepthIndex]))

NrStazUsedBest  = "%d" % int(NrSt[FirstDepthIndex])

print "\n\nSUMMARY BEST SOLUTION"
print "---------------------"
print "Epicenter:      ", Lat_epi, Lon_epi
print "Tested depts:   ", depths_test
#print "Best variance:  ", var[FirstDepthIndex], "at depth", depth[FirstDepthIndex], "[km]", "with", NrSt[FirstDepthIndex], "stations used"
print "Best variance:  ", var[FirstDepthIndex], "at depth", depth[FirstDepthIndex], "[km]", "with", NrStazUsedBest, "stations used"
print "Best depth:     ", Depth_uncertainty[0], "<-", depth[FirstDepthIndex], "->", Depth_uncertainty[1], "  <- Z_var uncertainty % =", DUnc
print "Best Mw:        ", Mw[UncIndex[0]], "<-", Mw[FirstDepthIndex], "->", Mw[UncIndex[1]]
print "Quality:        ", Quality, readme
depth_unce = 'Best depth:     ' + str(Depth_uncertainty[0]) + '<-' + str(depth[FirstDepthIndex]) + '->' + str(Depth_uncertainty[1]) + '  <- Z_var uncertainty % =' + str(DUnc)
mw_unc     = 'Best Mw:        ' + str(Mw[UncIndex[0]]) + '<-' + str(Mw[FirstDepthIndex]) + '->' + str(Mw[UncIndex[1]]) + '  <- Z_var uncertainty % =' + str(DUnc)
quality_lin= 'Quality:        ' + Quality + ' ' + readme

#print "\nBest Solution:" 
#print "----------------"
f = open(files[FirstDepthIndex], 'r')
for lines in f:
    print lines.rstrip()

#rename best solution
de = "%03d" % (depth[FirstDepthIndex])
shutil.copy2(files[FirstDepthIndex], args.invdir + os.sep + 'best_mt_' + de + '.out')
bestFit = findBestFit(args, depth[FirstDepthIndex])
for i in range(len(bestFit)):
    shutil.copy2(args.invdir + os.sep + bestFit[i], args.invdir + os.sep + 'best_' + bestFit[i])

outBest = open(args.invdir + os.sep + 'best_mt_' + de + '.out', 'a')
outBest.write(depth_unce + '\n')
outBest.write(mw_unc + '\n')
outBest.write(quality_lin)
outBest.close()

# Plot Summary solutions (solution - variance - depth)
plotFile = plotSolutions(files, var, pdc, Mxx, Myy, Mzz, Mxy, Mxz, Myz, stk, dip, slp, Mw, title, Quality, bestDepth, FirstDepthIndex, args)

print "\nPLOTS:"
print "------"
print "Best Solution Waveform fit         : ", args.invdir + os.sep + 'best_' + bestFit[i]
print "Variance <-> Depth <-> FocMech plot: ", args.invdir + os.sep + 'variance-depth.pdf'


print "\n\n=============================="
print "END AUTOMATIC PYTDMT PROCEDURE"
print "------------------------------"
endT = strftime("%Y-%m-%d %H:%M:%S", gmtime())
finT = datetime.now() 
difT = finT - iniT
print "End Time:", endT
print "Exec Time [sec]: ", difT.seconds
print "=============================="

