from xml.dom import minidom, Node
from xml.dom.minidom import parseString
import kmlmodule
import os,sys
from processData import getSTationslist
import subprocess

def createGMT(tr, mt, args):

    StazOb = getSTationslist(tr)

    GMT_PATH = "/sw/bin"

    #First Create list of station
    sta = open(args.invdir + os.sep + "used_stations.txt", "w")
    for i in range(len(StazOb)):
        sta.write("%s %s %s\n" % (tr[i*3].stats['station'], tr[i*3].stats['stla'], tr[i*3].stats['stlo']))
    sta.close()
    sta = open(args.invdir + os.sep + "epicenter.txt", "w")
    sta.write("%s %s" % (tr[i*3].stats['evlo'], tr[i*3].stats['evla']))
    sta.close()

    EvLat = float(tr[0].stats['evla'])
    EvLon = float(tr[0].stats['evlo'])
    dLat  = 10
    dLon  = 10
    minLat = str(EvLat - 10)
    maxLat = str(EvLat + 10)
    minLon = str(EvLon - 10)
    maxLon = str(EvLon + 10)
    pLat   = EvLat - 5
    pLon   = EvLon - 5

    mtEle  = str(EvLon) + " " + str(EvLat) + " 0 " + str(mt[31]) + " " + str(mt[32]) + " " + str(mt[33]) + \
             " 6 " + str(pLon) + " " + str(pLat)
#   mtEle  = str(EvLon) + " " + str(EvLat) + " 0 " + str(mt[31]) + " " + str(mt[32]) + " " + str(mt[33]) + " " + str(mt[34]) + " " + \
#            str(mt[35]) + " " + str(mt[36]) + " 1 1 " + str(pLon) + " " + str(pLat)
    

    # BEGIN Generating Shell
    # Define projection scale for the regional and local maps
    sh  = "#!/bin/sh\n"
    sh += GMT_PATH + os.sep + "gmtset ANNOT_FONT_SIZE_PRIMARY=8\n"
    sh += GMT_PATH + os.sep + "gmtset LABEL_FONT_SIZE=10\n"
    sh += GMT_PATH + os.sep + "gmtset HEADER_FONT_SIZE=16\n"
    sh += GMT_PATH + os.sep + "gmtset HEADER_OFFSET=0.0c\n"
    sh += GMT_PATH + os.sep + "gmtset BASEMAP_TYPE=plain\n\n"
    sh += "STATIONS=" + args.invdir + os.sep + "stations.txt\n"
    sh += "EPICENTER=" + args.invdir + os.sep + "epicenter.txt\n"

    # Regional 
    sh += "R=" + minLon + "/" + maxLon + "/" + minLat + "/" + maxLat +"\n"
    sh += "ech=15\n" 
    sh += "OUT=" + args.invdir + os.sep + args.pltname + "\n"
    sh += "PROJ=-JM$ech\n"

    sh += GMT_PATH + os.sep + "pscoast -R$R -W0.7p,black  $PROJ -Dh -N1 -BWSen2.0f1.0:.'" + args.title + "': -Glightgray -K -P -Y7 -X2 > $OUT.ps\n"
    sh += GMT_PATH + os.sep + "psxy $EPICENTER -R$R $PROJ -Sa0.9 -W2.1 -Gred -O -K >> $OUT.ps\n"
    sh += "/bin/cat $STATIONS | awk \'{print $3,$2}\' | \\\n"
    sh += GMT_PATH + os.sep + "psxy -R$R $PROJ -Sd0.4 -Ggreen -W0.1 -O -K >> $OUT.ps\n"
    sh += GMT_PATH + os.sep + "psmeca -R$R $PROJ -O -Sa0.7 -C0.5 << END >> $OUT.ps\n"
#   sh += GMT_PATH + os.sep + "psxy -R$R $PROJ -Sd0.4 -Ggreen -W0.1 -O -K >> $OUT.ps\n"
#   sh += GMT_PATH + os.sep + "psmeca -R$R $PROJ -O -Sa0.7 -C0.5 << END >> $OUT.ps\n"
    sh += mtEle + "\n"
    sh += "END\n"
   
    sh += "convert -quality 97 -density 220 $OUT.ps $OUT.jpg\n"
    sh += "convert -quality 97 -density 220 $OUT.ps $OUT.pdf\n"
    sh += "convert -resize x1024 $OUT.jpg $OUT.small.jpg\n"

    run=open(args.invdir + os.sep + "run_maps.csh", 'w')
    run.write("%s" % (sh))
    run.close()
    
    # chmod +x
    os.chmod(args.invdir + os.sep + "run_maps.csh", 0774)

#   aa = args.outdir + os.sep + "run_maps.csh"
#   shellscript = subprocess.Popen([aa], stdin=subprocess.PIPE)
#   returncode = shellscript.wait()

    return 0


def createKML(tr,outpath):

    StazOb = getSTationslist(tr)

    kmlDoc = minidom.Document()
    kmlElement = kmlmodule.createKMLElement(kmlDoc)
    documentElement = kmlmodule.createDocumentElement(kmlDoc)
    documentElement = kmlElement.appendChild(documentElement)


    list_stations=[1]

#   for i in range(len(tr)):

    placemarkElement = kmlmodule.createPlacemarkElement(kmlDoc,  "",  "Earthquake", "")
    lat = tr[0].stats['evla']
    lon = tr[0].stats['evlo']
    ele = 0
    coordinates = [[lon,lat,ele]]
    pointElement = kmlmodule.createPointElement(kmlDoc,  "",  0,  "", coordinates )
    placemarkElement.appendChild(pointElement)
    documentElement.appendChild(placemarkElement)

    for i in range(len(StazOb)):
        placemarkElement = kmlmodule.createPlacemarkElement(kmlDoc,  "",  tr[i*3].stats['station'], "")
        lat = tr[i*3].stats['stla']
        lon = tr[i*3].stats['stlo']
        ele = 0
        coordinates = [[lon,lat,ele]]
        pointElement = kmlmodule.createPointElement(kmlDoc,  "",  0,  "", coordinates )
        placemarkElement.appendChild(pointElement)
        documentElement.appendChild(placemarkElement)

    kmlFile = open(outpath + os.sep + 'stations.kml',  'w')
    kmlFile.write(kmlDoc.toprettyxml(' '))
    kmlFile.close()

    return 0
