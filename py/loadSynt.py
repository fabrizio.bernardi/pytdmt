import os
import sys
import glob
from obspy.core import Stream
from obspy.core import read
from syntlib import doFinalLibPart
from processData import getStationslist

def reorderLibGreen(gr, Sta, StDist):


    out = Stream()

    for i in range(len(Sta)):
        out.append(gr[10 * i + 3])      
        out.append(gr[10 * i + 2])      
        out.append(gr[10 * i + 6])      
        out.append(gr[10 * i + 5])      
        out.append(gr[10 * i + 4])      
        out.append(gr[10 * i + 9])      
        out.append(gr[10 * i + 8])      
        out.append(gr[10 * i + 7])      
        out.append(gr[10 * i + 0])      
        out.append(gr[10 * i + 1])      

    for i in range(len(Sta)):
        for n in range(0,10):
            out[10 * i + n].stats.dist = StDist[i]    

    return out


def getStationsDist(st):

    distkm = []
    for i in range(len(st)/3):
        dist = st[i*3].stats.dist 
        distkm.append(dist)

    return distkm

def findMinDiff(depthEarthquake, inf):

    depths = []
    for i in range(len(inf)):
        depths.append(inf[i])
        try:
           inf[i] = abs(float(inf[i])-depthEarthquake)
        except:
           pass

    aa=inf.index(min(inf))

    return depths[aa]

def findBestLibDepth(LibPath, finalLibPath, args):

    pi = args.epi.rsplit(' ')
    depthEarthquake = float(pi[2])

    subpath_elements = finalLibPath.rsplit(os.sep)

    depth_path = LibPath + os.sep + subpath_elements[0] + os.sep + subpath_elements[1]

    inf = os.listdir(depth_path)

    bestDepth = findMinDiff(depthEarthquake, inf)

    bestDepthPath = depth_path + os.sep + bestDepth + os.sep + subpath_elements[-3] \
                               + os.sep + subpath_elements[-2] + os.sep + subpath_elements[-1]

    return bestDepthPath

def findBestLibDists(bestLibDepth, StDist):

    # load ex1.sac
    aa =  glob.glob(os.path.join(bestLibDepth + os.sep ,'*.ex1.sac'))

    dists = []
    Synts = []
    exifi = [] 
    for i in range(len(aa)):
        foe = aa[i].split('-')
        dists.append(float(foe[7]))

    for i in range(len(StDist)):
        MaxD = 100000
        posiz = -1
        for j in range(len(dists)):
            if(abs(StDist[i] - dists[j]) <= MaxD):
               MaxD = abs(StDist[i] - dists[j])
               posiz = j
        exifi.append(aa[posiz])
   
    for i in range(len(exifi)):
        exifi[i] = exifi[i].replace("ex1","*")
        temp =  glob.glob(os.path.join(exifi[i]))
        for l in range(len(temp)):
            Synts.append(temp[l])

    return Synts

def loadLibSynt(bestSyntDists, args):

    Gr = Stream()

    for i in range(len(bestSyntDists)):

        tr = read(bestSyntDists[i])
        Gr.append(tr[0])

    return Gr

def loadGreensFromLib(st,args):

    StList = getStationslist(st) 

    StDist = getStationsDist(st)

    Sta = getStationslist(st)
 
    pi = args.epi.rsplit(' ')

    LibPath, finalLibPath = doFinalLibPart(pi[2], args.npts, args.delta, args)

    bestLibDepth = findBestLibDepth(LibPath, finalLibPath, args)

    bestSyntDists = findBestLibDists(bestLibDepth, StDist)


    greens = loadLibSynt(bestSyntDists, args)

    greens =  reorderLibGreen(greens,Sta, StDist)

    return greens
