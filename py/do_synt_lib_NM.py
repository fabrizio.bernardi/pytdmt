#!/opt/local/bin/python2.7

import os
import sys
import stat
import glob
import shutil
import os.path
import argparse
from obspy.core import read

def parseMyLine():
    parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter,description='Synthetics generator\n------------\n')
    parser.add_argument('--model',default='None',help='Earth model for greenfunctions. Default=None. See README_MODEL.txt for details')
    parser.add_argument('--depth',default='10',help='Source depth. Default=10')

    parser.add_argument('--npts',default='512',help='Number of points for greens. Power of 2. Default=1024')
    parser.add_argument('--delta',default='0.5',help='Sampling interval in seconds for greens. Default=0.5')
    parser.add_argument('--cpus',default='1',help='Number of CPU available for greens computation. Min=1, Max=4. Default=1')
    parser.add_argument('--rvel',default='8',help='Reduction velocity. Recommendet value = 8km/s. Default=8')

    parser.add_argument('--libpath', default='pytdmt', help='Main lib directory. Default: $PATH_TO_PYTDMT/lib')
    parser.add_argument('--range', default='100 200', help='Distance range for syntetics in km. Default=100 200')
    parser.add_argument('--dkm', default='10', help='Distance intervall for syntetics in km. Default=10')

    parser.add_argument('--filtered',default='N', help='Apply taper and filter to the syntetics. \
                           !!! When Y these parameters must be used in pytdmt analysis. Default=Y')
    parser.add_argument('--bandpass',default='0', help='Bandpass filter "corners fimn fmax". No Defaults. E.g.: "2 0.01 0.1"')
    parser.add_argument('--highpass',default='0', help='Highpass filter "corners freq". No Defaults. E.g.: "2 0.01"')
    parser.add_argument('--lowpass',default='0', help='Lowpass filter "corners freq". No Defaults. E.g.: "2 0.1"')
    parser.add_argument('--zeroph',default='False', help='Zerophase for high, low and bandpass. True/False. Defaul:False')
    parser.add_argument('--taper',default='0.1', help='cos Taper. If taper=-1 no taper is applied. Defaults=0.1')
    parser.add_argument('--sim',default='PZs', help='Remove instrument method: PZs for poles and zeros, RESP, for RESP_ files. Default=PZs')
    parser.add_argument('--flim',default='0.002 0.005 0.5 1', help='Corner frequency for deconvolution filtering. Defaults 0.002 0.005 0.5 1')
    parser.add_argument('--dva',default='1', help='Data type: 1(displacement); 2(velocity); Default = 1')
    parser.add_argument('--pre', default='0', help='Length of signal in seconds Before event origin Time.')

    args=parser.parse_args()

    return args

def which(name):
    found = 0
    out_path = 'none'
    for path in os.getenv("PATH").split(os.path.pathsep):
        full_path = path + os.sep + name
        if os.path.exists(full_path):
            """
            if os.stat(full_path).st_mode & stat.S_IXUSR:
                found = 1
                print(full_path)
            """
            found = 1
            out_path = full_path
            break

    return full_path

def checkConsistency(self):

    if(self.bandpass != '0'):
       li = self.bandpass.split()
       if(li[2] <= li[1]):
          print "bandpass filter: Use nrCorners min_freq max_freq"
          sys.exit()

    if self.model != "None":
       aa=os.path.exists(self.model)
       if aa == False:
          print "Model --model", self.model, "does not exists"
          sys.exit()
    else:
       print "Model file unknown. Use --model option"
       sys.exit()

def rangeToList(args):

    D    = 0
    kms  = []
    dkm  = int(float(args.dkm)) * 1
    dist = args.range.split()
    dmin = int(float(dist[0])) * 1
    dmax = int(float(dist[1])) * 1 
    D    = int(float(dist[0])) * 1
    while D <= dmax:
          kms.append(D)
          D = D + dkm

    return kms

def doFinalLibPart(args):

    # If args.libpath == pytdmt libraries are installed into
    # the actual runin pytdmt libarry
    # 
    mainPath = which('pytdmt.py')
#   print "----",mainPath
    mainPath = mainPath.replace(os.sep + 'py' + os.sep + 'pytdmt.py','')
    if (args.libpath == 'pytdmt'):
       mainPath = mainPath + os.sep + 'lib'
    else:
       mainPath = mainPath + os.sep + args.libpath

    # take model earth
    foe = args.model.split('/')
    EModel = foe[-1]

    # create main lib if not exists
    if (os.path.exists(mainPath) == False):
       try:
         os.makedirs(mainPath)
       except:
         print "Unable to create directory " + mainPath

    # generate work temp-subpath
#   print mainPath
    if (os.path.exists(mainPath + os.sep + 'WORK') == False):
       try:
         os.makedirs(mainPath + os.sep + 'WORK')
       except:
         print "Unable to create directory " + mainPath + os.sep + 'WORK'


    # based on the greens parameters (delta, npts, dkm, depth .. etc,
    # create subdirectories
    # structure
    # Earth model (PREM)
    # filter(unfiltered/001-002)
    # depth (010)
    # npts  (02048)
    # delta 0050
    if (args.filtered != 'Y'):
       f_mod = 'unfiltered'
    else:
       fee = args.bandpass.split()
       f_mod = "%1d-%.3f-%.3f" % (int(fee[0]),float(fee[1]), float(fee[2]))
    d_mod = "%03d" % (depth)
    n_mod = "%05d" % (npts)
    D_mod = "%04d" % (delta * 100)
    p_mod = "%03d" % (int(args.pre))

    subPath = EModel + os.sep + f_mod + os.sep + d_mod + os.sep + n_mod + os.sep + p_mod + os.sep + D_mod

    print mainPath + os.sep + subPath

    return mainPath, subPath

#####################################################################
#####################################################################
args=parseMyLine()
ll = sys.argv[1:]
if not ll:
       print "Use -h or --help option for Help"
       sys.exit(0)

checkConsistency(args)

# define main parameters:
delta      = float(args.delta)
nrcpu      = float(args.cpus)
npts       = float(args.npts)
rvel       = float(args.rvel)
depth      = float(args.depth)
earthModel = args.model

# take model earth
foe = args.model.split('/')
EModel = foe[-1]

dist_list = rangeToList(args)

#dist_list = [100, 200, 300]
print dist_list

LibPath, finalLibPath = doFinalLibPart(args)

# Generate path
if (os.path.exists(LibPath + os.sep + finalLibPath) == False):
   try:
      os.makedirs(LibPath + os.sep + finalLibPath)
   except:
      print "Unable to create directory " + LibPath + os.sep + finalLibPath

# for each range of distance generate a stationfile in LibPath/WORK
# compute greens and move to final path

os.chdir(LibPath + os.sep + 'WORK')

for i in range(len(dist_list)):

    f = open(LibPath + os.sep + 'WORK' + os.sep + 'temp.dist', 'w')
    str = "%.1f %.2f %d 0 0" % (dist_list[i], delta, npts)
    f.write(str)
    f.close()
    
    exec1 = 'sprep96 -M ' + earthModel + ' -d ' + LibPath + os.sep + 'WORK' + os.sep + 'temp.dist -L -R -NMOD 2000 -HR 0 -HS ' + args.depth
    exec2 = 'spulse96 -p -V -l 4 -d ' + LibPath + os.sep + 'WORK' + os.sep + 'temp.dist | f96tosac -B'
    print exec1
#   print exec2

    os.system(exec1)
    os.system('sdisp96')
    os.system('sregn96')
    os.system('slegn96')
    os.system('slegn96')
    os.system(exec2)

    sacs = glob.glob("*.sac")
 
    for j in range(len(sacs)):
        st = read(sacs[j])
        d_name = "%03d" % (depth)
	n_name = "%05d" % (int(args.npts))
	D_name = "%06.2f" % (float(args.delta))
        p_name = "%03d" % (int(args.pre))
#       dkm    = "%08.2f" % float(st[j].stats.dist)
        dkm    = "%08.2f" % dist_list[i] 

        st[0].stats.channel = st[0].stats.channel.lower()
        if(st[0].stats.channel == 'zex'):
           st[0].stats.channel = 'ex1'
        if(st[0].stats.channel == 'rex'):
           st[0].stats.channel = 'ex2'
        st[0].stats.dist    = dkm
        if(st[0].stats.channel == 'zvf' or st[0].stats.channel == 'rvf' or \
           st[0].stats.channel == 'zhf' or st[0].stats.channel == 'rhf' or \
           st[0].stats.channel == 'thf' or st[0].stats.channel == 'xhf'):
           os.remove(sacs[j])

        else:
           st[0].stats.channel = st[0].stats.channel.replace('r','x')
           Name   = 'green' + '.' + EModel + '.Depth-' + d_name + '-.Npts-' + n_name + '-.Delta-' + D_name + '-.Dist-' + dkm + '-.' + st[0].stats.channel + '.sac'
#       green[i].write(LibPath + os.sep + finalLibPath + os.sep + Name, format='SAC')
#       print st[0].stats.npts, st[0].stats.delta, st[0].stats.channel, st[0].stats.dist, finalLibPath + os.sep + Name
           shutil.move(sacs[j], LibPath + os.sep + finalLibPath + os.sep + Name)

#   sys.exit()



